
const slugify= require("slugify");
const express= require ("express");

const app= express ();

const titulo= "Ug0104";
app.set("view engine", "ejs");



const productos=[
    {id:1, nombre:"Medialuna", precio: 5000, slug:"peso, boton, dpi"},
    {id:2, nombre:"Alfajor", precio: 3000, slug:"peso, boton, dpi"},
    {id:3, nombre:"Pastafrola", precio: 5500, slug:"peso, botos, dpi"},
    {id:4, nombre:"Factura", precio: 3500, slug:"peso, boes, dpi"},
    {id:5, nombre:"Chipa", precio: 2000, slug:"peso, bones, di"},
    {id:6, nombre:"Bollo", precio: 3000, slug:"po, botones, dpi"},
    {id:7, nombre:"Galletita", precio: 2500, slug:"pso, botones, dpi"},
];

productos.forEach(productos=>{
    productos.slug=slugify(productos.nombre)
});



app.set("views", "views");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//app.get(("/bienvenida"), (req, res) => {res.send("Hola mundo")})

app.get(("/bienvenida"), (req, res) => {res.render("bienvenida", {titulo:titulo, productos:productos})})

app.get("/det_producto/:slug", (req, res)=> {
    const slug= req.params.slug;
    const productoEncontrado=productos.find(producto=>producto.slug===slug)
    res.render("det_producto", {titulo:titulo, producto: productoEncontrado});
})


app.put("/det_producto/:slug", (req, res) => {
    const slug = req.params.slug;
    const productoEncontrado = productos.find(producto => producto.slug === slug);
  
    if (productoEncontrado) {
        productoEncontrado.nombre = req.body.nombre;
        productoEncontrado.precio = parseFloat(req.body.precio);
  
        productos.forEach(producto => {
          producto.slug = slugify(producto.nombre);
        });

        res.json({ success: true, slug: productoEncontrado.slug });
    } else {
        res.status(404).send("Producto no encontrado");
    }
});

app.delete("/det_producto/:slug", (req, res) => {
    const slug= req.params.slug;
    const elimProducto = productos.findIndex(producto => producto.slug === slug);

    if (elimProducto !== -1){
        productos.splice(elimProducto, 1);

        res.status(200).json({mensaje:"Producto eliminado"});
    }else{
        res.status(404).json({mensaje: "Producto no encontrado"});
    };
  
});


app.get("/crear_producto", (req, res) => {
    res.render("crear_producto", { titulo: titulo });
});

app.post("/crear_producto", (req, res) => {
     
    const nuevoProducto = {
        id: (Math.max(...productos.map((producto)=>producto.id))+1),
       
        nombre: req.body.nombre,
        precio: parseFloat(req.body.precio),
        slug: slugify(req.body.nombre)
    };

    productos.push(nuevoProducto);
    res.redirect("/bienvenida");
});

app.listen(3000, () => {
    console.log("Servidor corriendo en el puerto 3000")
});
